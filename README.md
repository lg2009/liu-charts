# liu-charts

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 组件在components下的charts文件夹，可以拷贝到您项目即可使用

### 适用场景
我们平时在使用 echarts 的时候，我们需要用  echarts.init() 来创建 echart 对象，    
然后还要使用 echart.setoption() 加载参数，    
还要根据屏幕的大小或者外部宽高的变化重新加载，    
使用起来比较麻烦，写法较为复杂    

**现在我们只需要一个组件就可以实现这些功能，我们只需要关心数据的变化即可**
**我们现在对echarts做了一些简单的封装，让新手使用起来非常方便。**
**<font color="red">echarts 版本5.2.1</font>**

### 使用方法
#### 安装
`npm  i liu-charts`
#### 使用方法
```
// main.js中
import LiuCharts from "liu-charts"
Vue.use(LiuCharts)

// 页面上
<liu-charts :options="chartOptions" ref="chart"/>
export default {
	data(){
		return{
			chartOptions:{
				xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                },
                yAxis: {
                    type: 'value'
                },
                series: [
                    {
                        data: [820, 932, 901, 934, 1290, 1330, 1320],
                        type: 'line',
                        areaStyle: {}
                    }
                ]
			}
		}
	}
}
```
### 如果需要使用到 echarts对象
`this.$refs.chart.echarts` 
### 如果您想使用图表的chart对象
`this.$refs.chart.chart`
### npm地址
`https://www.npmjs.com/package/liu-charts`
### gitee地址
`https://gitee.com/lg2009/liu-charts`