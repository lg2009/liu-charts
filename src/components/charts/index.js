import BaseCharts from "./BaseCharts"
const install = (Vue) => {
    Vue.component(BaseCharts.name, BaseCharts)
}
export default install